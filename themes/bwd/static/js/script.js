/*!
 * jQuery TextChange Plugin
 * http://www.zurb.com/playground/jquery-text-change-custom-event
 *
 * Copyright 2010, ZURB
 * Released under the MIT License
 */
 (function(a){a.event.special.textchange={setup:function(){a(this).data("lastValue",this.contentEditable==="true"?a(this).html():a(this).val());a(this).bind("keyup.textchange",a.event.special.textchange.handler);a(this).bind("cut.textchange paste.textchange input.textchange",a.event.special.textchange.delayedHandler)},teardown:function(){a(this).unbind(".textchange")},handler:function(){a.event.special.textchange.triggerIfChanged(a(this))},delayedHandler:function(){var c=a(this);setTimeout(function(){a.event.special.textchange.triggerIfChanged(c)},
 25)},triggerIfChanged:function(a){var b=a[0].contentEditable==="true"?a.html():a.val();b!==a.data("lastValue")&&(a.trigger("textchange",[a.data("lastValue")]),a.data("lastValue",b))}};a.event.special.hastext={setup:function(){a(this).bind("textchange",a.event.special.hastext.handler)},teardown:function(){a(this).unbind("textchange",a.event.special.hastext.handler)},handler:function(c,b){b===""&&b!==a(this).val()&&a(this).trigger("hastext")}};a.event.special.notext={setup:function(){a(this).bind("textchange",
 a.event.special.notext.handler)},teardown:function(){a(this).unbind("textchange",a.event.special.notext.handler)},handler:function(c,b){a(this).val()===""&&a(this).val()!==b&&a(this).trigger("notext")}}})(jQuery);

(function ($) {  //start closure
	$(document).ready(function() {
		$('.perkplant-input').bind('textchange', function (event, previousText) {
			if ($(this).val().search(/^\d*$/) == -1) {  // tests if field consists of only digits
				$(this).val(previousText);  // if not, replaces with previous value
			}
			var total=0;
			$('.perkplant-input').each(function() {
				var number = $(this).val();
				if (number == "") {
					number=0;
				}  // empty fiels are treated as zero
				var price = $(this).attr('data-item-price');
				// calculate total
				total=total+parseInt(number)*parseFloat(price);
			}); // end each
			$('#total').val(total.toFixed(2));  // update "total" field
			//TODO: disable total field, so that users cannot change the value
		}); // end event binding
		
		// form submission via JavaScript
		$('#perkplanten-submit').click(function (e) {
			// form validation
			var error = false;
			var name = $('#naam-leerling').val();
			var klas = $('#klas').val();
			var email = $('#email').val();
			if (name.length == 0) {
				var error = true;
				$('#naam-leerling').css("border-color", "#D8000C");
			} else {
				$('#naam-leerling').css("border-color", "#666");
			}
			if (klas.length == 0) {
				var error = true;
				$('#klas').css("border-color", "#D8000C");
			} else {
				$('#klas').css("border-color", "#666");
			}
			if (email.length == 0 || email.indexOf('@') == '-1') {
				var error = true;
				$('#email').css("border-color", "#D8000C");
			} else {
				$('#email').css("border-color", "#666");
			}
			if (error == false) {
				//disable the submit button to avoid spamming
				//and change the button text to Sending...
				$('#perkplanten-submit').attr({
					'disabled': 'false',
					'value': 'Bestelling wordt verstuurd...'
				});
			}

		//stop the form from being submitted
		e.preventDefault();

			///* using the jquery's post(ajax) function and a lifesaver
			//function serialize() which gets all the data from the form
			//we submit it to send_email.php */
			//$.post("/perkplanten.php", $("#perkplanten-form").serialize(), function (result) {
				////and after the ajax request ends we check the text returned
				//if (result == 'sent') {
					////if the mail is sent remove the submit paragraph
					//$('#cf-submit').remove();
					////and show the submit success div with fadeIn
					//$('#submit-success').fadeIn(500);
				//} else {
					////show the submit failed div
					//$('#submit-fail').fadeIn(500);
					////re enable the submit button by removing attribute disabled and change the text back to Send The Message
					//$('#perkplanten-submit').removeAttr('disabled').attr('value', 'Versturen');
				//}
			//});
		//}
	});

	});  // end document ready
}(jQuery));  // end closure
