<?php
function logg($message, $multiline = FALSE) {
	////specify path to log file here
	//$logfile = "/data/sites/web/ulmusbe/data/log.txt";
	////add timestamp prefix
	//$prefix = date('Y/m/d H:i:s') . ": ";
	//if ($multiline) {
		////add delimiter for easier reading
		//$delimiter = "\n****************************************************************************\n\n";
		//$message = $delimiter . $prefix . "\n" . $message . $delimiter;
	//} else {
		//$message = $prefix . $message . "\n";
	//}
	////append to log file
	//file_put_contents($logfile , $message, FILE_APPEND);
}

function ajax_ok() {
	 // Bypass for localhost (127.0.1.1)
	 if (isset($_SERVER['SERVER_ADDR'])) {
		 if ($_SERVER['SERVER_ADDR'] == '127.0.1.1') {
			 logg("Using localhost (" . $_SERVER['SERVER_ADDR'] . ")");
			 return TRUE;
		 }
	 } else {
		 logg("Error: No Address header");
		 return FALSE;
	}
	//check referer
	if (isset($_SERVER['HTTP_REFERER'])) {
		$address = 'https://' . $_SERVER['SERVER_NAME'];
		if (strpos($_SERVER['HTTP_REFERER'], $address) !== 0) {
			logg("Error: Invalid referer header (" . $_SERVER['HTTP_REFERER'] . ")");
			return FALSE;
		}
	} else {
		logg("Error: No referer header");
		return FALSE;
	}
	//Only process POST requests (see below)
	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		return TRUE;
	} else {
		logg("Error: Not a POST action (" . $_SERVER['REQUEST_METHOD'] . ")");
		return FALSE;
	}
}

function no_bots() {
	if ( !empty($_POST["email2"]) ) { // dumass spam bot submitted this form (honeypot approach)
		logg('Spam attempt by ' . $_SERVER['REMOTE_ADDR']);
		return false;
	}
	return true;
}

// Only process valid AJAX requests, not submitted by spam bots
if ( ajax_ok() && no_bots() ) {
	print_r($_POST);
}


?>
